import heapq 

adjacency_list = {
    # From city 'a', there are flights to cities 'b', 'd' and 'm'. These
    # flights cost 3, 11 and 11 respectively:
    'a': {'b': 3, 'd': 11, 'm': 11},
    # Similarly for the rest of the cities:
    'b': {'a': 3, 'c': 19, 'e': 3, 'h': 1},
    'p': {'c': 11, 'h': 3, 'i': 3},
    'c': {'g': 7, 'j': 5, 'q': 3},
    'd': {'h': 7, 'i': 5},
    'e': {'b': 3, 'm': 1, 'r': 11},
    'f': {'k': 3, 'q': 3},
    'g': {'c': 7, 'l': 3},
    'h': {'b': 1, 'p': 3, 'd': 7},
    'i': {'p': 3, 'd': 5, 'o': 3},
    'j': {'c': 5, 'o': 1},
    'k': {'f': 3, 'l': 1},
    'l': {'g': 3, 'k': 1, 'r': 11, 's': 3},
    'm': {'a': 11, 'e': 1, 'n': 1},
    'n': {'m': 1, 'o': 3},
    'o': {'i': 3, 'j': 1, 'n': 3},
    'q': {'c': 3, 'f': 3, 's': 11},
    'r': {'e': 11, 'l': 11},
    's': {'l': 3, 'q': 11}
}

def solve(adjacency_list, origin, destination):
    
    # Initialize distances and previous nodes for all nodes
    distances = {node: float('inf') for node in adjacency_list}
    previous_nodes = {node: None for node in adjacency_list}
    
    # Set the distance of the origin node to 0
    distances[origin] = 0
    
    # Initialize the priority queue with the origin node
    pq = [(0, origin)]
    
    while pq:
        # Get the node with the minimum distance from the origin from the priority queue
        current_distance, current_node = heapq.heappop(pq)
        
        # If we've reached the destination node, backtrack to find the shortest path and return it
        if current_node == destination:
            shortest_path = []
            while current_node is not None:
                shortest_path.append(current_node)
                current_node = previous_nodes[current_node]
            shortest_path.reverse()
            return (current_distance, shortest_path)
        
        # Otherwise, update the distances of its neighbors and add them to the priority queue
        for neighbor, weight in adjacency_list[current_node].items():
            distance = current_distance + weight
            if distance < distances[neighbor]:
                distances[neighbor] = distance
                previous_nodes[neighbor] = current_node
                heapq.heappush(pq, (distance, neighbor))
    
    # If we've visited all reachable nodes and haven't reached the destination, there's no path
    return None


# if __name__ == '__main__':
#     print(solve(adjacency_list, 'a', 's') or "No path found...")

result = solve(adjacency_list, 'a', 's')
if result is not None:
    distance, path = result
    print(f"Shortest path distance: {distance}")
    print(f"Shortest path: {' -> '.join(path)}")
else:
    print("No path found...")