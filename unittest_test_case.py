import unittest
from main import *



class YourTestCaseName(unittest.TestCase):
    def test_count_char_repeats_normal_inputs(self):
        # Test normal inputs
        self.assertEqual(count_char_repeats("hello"), 1)
        self.assertEqual(count_char_repeats("aabbcc"), 3)
        self.assertEqual(count_char_repeats("aabbbbcccc"), 7)
        self.assertEqual(count_char_repeats("a"), 0)

    def test_count_char_repeats_empty_string(self):
        self.assertEqual(count_char_repeats(""), 0)

    def test_count_char_repeats_all_same_characters(self):
        self.assertEqual(count_char_repeats("cccc"), 3)
        self.assertEqual(count_char_repeats("eeeeeeeeeeee"), 11)

    def test_count_char_repeats_alternating_characters(self):
        self.assertEqual(count_char_repeats("ababab"), 0)
        self.assertEqual(count_char_repeats("abababcc"), 1)
        self.assertEqual(count_char_repeats("aabbaabbaabb"), 6)

    def test_count_char_repeats_special_characters(self):
        self.assertEqual(count_char_repeats("hello!!!"), 3)
        self.assertEqual(count_char_repeats("Hello"), 1)
        self.assertEqual(count_char_repeats("HellO"), 1)
        self.assertEqual(count_char_repeats("hello "), 1)

    def Test_type(self):
        self.assertEqual(TypeError, lambda: count_char_repeats(True))
        self.assertEqual(TypeError, lambda: count_char_repeats([50, 100]))
        self.assertEqual(TypeError, lambda: count_char_repeats(0.0))

class YourTestCaseName(unittest.TestCase):
    def test_string_similarity(self):
        # Test similarity to de edge inputs
        self.assertEqual(string_similarity("hello", "hello"), 5)
        self.assertEqual(string_similarity("hello", "world"), 1)
        self.assertEqual(string_similarity("hello", "he"), 2)
        self.assertEqual(string_similarity("", ""), 0)
        self.assertEqual(string_similarity("hello", ""), 0)
        self.assertEqual(string_similarity("abcd", "efgh"), 0)
        self.assertEqual(string_similarity("Hello", "hEllo"), 5)
        self.assertEqual(string_similarity("CAT", "cat"), 3)

    def Test_type(self):
        self.assertEqual(TypeError, lambda: string_similarity(True))
        self.assertEqual(TypeError, lambda: string_similarity([50, 100]))
        self.assertEqual(TypeError, lambda: string_similarity(0.0))

class YourTestCaseName(unittest.TestCase):
    def test_stringify_time_ago(self):
        self.assertEqual(stringify_time_ago(45), "45 seconds")
        self.assertEqual(stringify_time_ago(89), "1 minute")
        self.assertEqual(stringify_time_ago(100), "2 minutes")
        self.assertEqual(stringify_time_ago(120), "2 minutes")
        self.assertEqual(stringify_time_ago(4000), "1 hour")
        self.assertEqual(stringify_time_ago(8600000), "100 days")
        
        # edge case for an hour
        self.assertEqual(stringify_time_ago(1799), "30 minutes")
        self.assertEqual(stringify_time_ago(1800), "1 hour")
        self.assertEqual(stringify_time_ago(5399), "1 hour")
        self.assertEqual(stringify_time_ago(5400), "2 hours")
        
        # edge case for a day
        self.assertEqual(stringify_time_ago(86399), "1 day")
        self.assertEqual(stringify_time_ago(86400), "1 day")
        self.assertEqual(stringify_time_ago(86401), "1 day")
        self.assertEqual(stringify_time_ago(172799), "2 days")

    def Test_type(self):
        self.assertEqual(TypeError, lambda: stringify_time_ago(True))
        self.assertEqual(TypeError, lambda: stringify_time_ago([50, 100]))
        self.assertEqual(TypeError, lambda: stringify_time_ago(0.0))
