import priority_queue_tests
import heapq

class HeapqPriorityQueue():

    def __init__(self):
        self.data = []

    def add(self, value):
        """Add `value` to the priority queue."""
        heapq.heappush(self.data, value)

    def fetch_smallest(self):
        """Find the smallest value in the priority queue, remove it from the
        queue and return it."""
        return heapq.heappop(self.data)

    def is_empty(self):
        """Returns `True` if the queue is empty, and `False` otherwise."""
        return len(self.data) == 0


if __name__ == '__main__':
    priority_queue_tests.run_all_tests(HeapqPriorityQueue)
