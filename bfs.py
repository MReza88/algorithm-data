from mazes import maze1, maze2
from collections import deque


def solve(maze):
    """Finds the shortest solution for the given `maze` from the top left square to the bottom right
    square, and mark the found path with 'o' characters. Returns a boolean indicating if a path was found.
    """

    height = len(maze)
    width = len(maze[0])
    origin = (0, 0) # (x,y)
    destination = (width-1, height-1) # (x,y)

    # Initialize the queue with the origin
    queue = deque()
    queue.append(origin)

    # Keep track of the previous square for each square that is put in the queue
    prev = {}
    prev[origin] = None

    # Keep track of visited squares
    visited = set()
    visited.add(origin)

    # Breadth-first search loop
    while queue:
        # Get the next square to explore from the queue
        curr = queue.popleft()
        draw_on_maze(maze, curr, '.')

        # Check if we have reached the destination
        if curr == destination:
            # Mark the path with 'o' characters
            while curr is not None:
                draw_on_maze(maze, curr, 'o')
                curr = prev[curr]
            return True

        # Explore the neighbors of the current square
        for neighbor in get_neighbors(maze, curr):
            if neighbor not in visited:
                queue.append(neighbor)
                visited.add(neighbor)
                prev[neighbor] = curr

    # No path found
    return False


def get_neighbors(maze, square):
    """Returns a list of all unvisited neighboring squares of the given `square` in the `maze`.
    """
    x, y = square
    neighbors = []

    # Check the right neighbor
    if x < len(maze[0]) - 1 and maze[y][x+1] != '#':
        neighbors.append((x+1, y))

    # Check the bottom neighbor
    if y < len(maze) - 1 and maze[y+1][x] != '#':
        neighbors.append((x, y+1))

    # Check the left neighbor
    if x > 0 and maze[y][x-1] != '#':
        neighbors.append((x-1, y))

    # Check the top neighbor
    if y > 0 and maze[y-1][x] != '#':
        neighbors.append((x, y-1))

    return neighbors


def draw_on_maze(maze, position, char):
    x, y = position
    maze[y] = maze[y][0:x] + char + maze[y][x+1:]


def print_maze(maze):
    # Ansi escape sequences, for showing the path as red
    FG_RED = '\x1b[31m'
    FG_RESET = '\x1b[39m'
    maze_text = "\n".join(maze)
    path_len = maze_text.count("o")
    explored = maze_text.count(".")

    maze_text = maze_text.replace('o', FG_RED+'o'+FG_RESET)
    print(f"Path length: {path_len}\nOther squares explored: {explored}\n\n{maze_text}\n\n\n")


if __name__ == '__main__':
    for maze_num, maze in [(1,maze1), (2,maze2)]:
        print(f"----- Maze {maze_num} -----\n")
        if solve(maze):
            print_maze(maze)
        else:
            print("No path.\n\n")
