import random
import time
import math
import colorama
import heapq

def generate_maze(size=24, seed = None):
    """Create a new random maze to be solved.

    Args:
        size (int): The width and height of the maze.
        seed (int): The random seed; when create_maze is called twice with the same size and seed,
            the exact same maze list will be returned.

    Returns:
        A two dimensional list of costs associated with the squares, order `[row][column]`. When
        the cost is `0`, there is a blocking element in this square.
    """
    if seed != None:
        random.seed(seed)

    squares = []
    
    for _ in range(size):
        row = []
        for _ in range(size):
            row.append(random.randint(1,size**2) if random.randint(0,3) else 0)
        squares.append(row)

    # Make sure the start and end squares are not blocked
    squares[0][0] = 1
    squares[-1][-1] = 2

    return squares


def print_maze(squares, path=None, show_prices=False):
    """Print a (solved) maze to the terminal.

    Args:
        squares (list(list(int))): The maze.
        path (list(tuple)): An optional list of (x,y) tuples that forms a path to be shown.
        show_prices (boolean): Show the price for each square. This makes the printed maze a lot wider.
    """

    size = len(squares)
    price_width = math.ceil(math.log10(size**2))

    reset = colorama.Style.RESET_ALL
    print(reset)
    for y,row in enumerate(squares):
        for x,price in enumerate(row):

            if price==0:
                color = colorama.Back.RED + colorama.Fore.BLACK
            elif path != None and (x,y) in path:
                color = colorama.Back.GREEN + colorama.Fore.BLACK
            else:
                color = colorama.Back.BLACK + colorama.Fore.WHITE

            if show_prices:
                number_str = (f"%0{price_width}d" % price) if price else (" " * price_width)
                print(f"{color}{number_str}{reset} ", end="")
            else:
                print(f"{color} ", end="")
        print(reset)
    print("")


def demo(size=24, seed=None, teleports=False):
    """Generates a maze, solves it and prints the results.

    Args:
        size (int): Width and height of the maze to demo.
        seed (int): Random seed of the maze to generate.
        teleports (boolean): Allow teleports between squares with the same price.
    """

    if seed == None:
        seed = int(time.perf_counter()) % 10000

    print(f"Solving {size}x{size} seed {seed} {'with' if teleports else 'without'} teleports...")

    maze = generate_maze(size, seed)

    start_time = time.perf_counter()
    path = solve_with_teleports(maze) if teleports else solve(maze)
    elapsed = time.perf_counter() - start_time

    if size <= 100:
        print_maze(maze, path, size<=24)

    if path:
        print(f"Cheapest path: {path} (length {len(path)})")
    else:
        print(f"No path found")
        
    print(f"Solver took {round(elapsed, 3)}s\n\n\n")


def solve(maze):
    size = len(maze)
    start = (0, 0)
    end = (size - 1, size - 1)

    # Initialize distance and path dictionaries
    distance = {start: maze[0][0]}
    path = {start: [start]}

    # Initialize priority queue with start node
    queue = [(maze[0][0], start)]

    while queue:
        current_cost, current_node = heapq.heappop(queue)

        # Check if reached the end
        if current_node == end:
            return path[current_node]  # Return the path to the end node

        # Explore neighboring nodes
        neighbors = get_neighbors(current_node, maze)
        for neighbor in neighbors:
            neighbor_cost = maze[neighbor[1]][neighbor[0]]
            new_cost = current_cost + neighbor_cost

            # Update distance and path if a shorter path is found or neighbor is unvisited
            if neighbor not in distance or new_cost < distance[neighbor]:
                distance[neighbor] = new_cost
                heapq.heappush(queue, (new_cost, neighbor))
                path[neighbor] = path[current_node] + [neighbor]

    return None  # No path found


def get_neighbors(node, maze):
    # Function to get neighboring nodes of a given node in the maze
    size = len(maze)
    x, y = node
    neighbors = []

    # Check for valid neighboring nodes
    if x > 0 and maze[y][x - 1] != 0:
        neighbors.append((x - 1, y))
    if x < size - 1 and maze[y][x + 1] != 0:
        neighbors.append((x + 1, y))
    if y > 0 and maze[y - 1][x] != 0:
        neighbors.append((x, y - 1))
    if y < size - 1 and maze[y + 1][x] != 0:
        neighbors.append((x, y + 1))

    return neighbors


def solve_with_teleports(maze):
    size = len(maze)
    start = (0, 0)
    end = (size - 1, size - 1)

    # Initialize distance and path dictionaries
    distance = {start: maze[0][0]}  # Dictionary to store the minimum distance to each node
    path = {start: [start]}  # Dictionary to store the path to each node
    teleport_positions = {}  # Dictionary to store positions with the same price for teleportation

    # Initialize priority queue with start node
    queue = [(maze[0][0], start)]  # Priority queue to explore nodes based on their cost

    # Store positions with the same price in teleport_positions dictionary
    for y in range(size):
        for x in range(size):
            price = maze[y][x]
            if price != 0:
                if price in teleport_positions:
                    teleport_positions[price].append((x, y))
                else:
                    teleport_positions[price] = [(x, y)]

    while queue:
        current_cost, current_node = heapq.heappop(queue)

        # Check if reached the end
        if current_node == end:
            return path[current_node] # Return the path to the end node

        # Explore neighboring nodes
        neighbors = get_neighbors(current_node, maze)
        for neighbor in neighbors:
            neighbor_cost = maze[neighbor[1]][neighbor[0]]
            new_cost = current_cost + neighbor_cost

            # Update distance and path if a shorter path is found or neighbor is unvisited
            if neighbor not in distance or new_cost < distance[neighbor]:
                distance[neighbor] = new_cost
                heapq.heappush(queue, (new_cost, neighbor))
                path[neighbor] = path[current_node] + [neighbor]

        # Check for teleportation possibility
        if current_cost > 0:
            teleports = teleport_positions.get(maze[current_node[1]][current_node[0]], [])
            for teleport in teleports:
                teleport_cost = maze[teleport[1]][teleport[0]]
                new_cost = current_cost + teleport_cost

                # Update distance and path for teleportation
                if teleport not in distance or new_cost < distance[teleport]:
                    distance[teleport] = new_cost
                    heapq.heappush(queue, (new_cost, teleport))
                    path[teleport] = path[current_node] + [teleport]

    return None # No path found


if __name__ == "__main__":
    print("------ Solvable mazes, without teleports ------\n")
    demo(3, 122)
    demo(7, 120)
    demo(10, 39047)
    demo(24, 3305)
    demo(100, 595)
    demo(250, 128)
    demo(1000, 129)

    print("------ Unsolvable mazes, without teleports ------\n")
    demo(3, 5)
    demo(24, 126)

    print("------ Solvable mazes, with teleports ------\n")
    demo(10, 39047, True)
    demo(24, 3305, True)
    demo(100, 595, True)
    demo(1000, 129, True)
