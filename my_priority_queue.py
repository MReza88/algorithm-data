import priority_queue_tests

class MyPriorityQueue():

    def __init__(self):
        self.data = [] # you are allowed to use a different data structure here if you want

    def add(self, value):
        """Add `value` to the min-priority queue."""
        self.data.append(value)
        self._bubble_up(len(self.data) - 1)

    def fetch_smallest(self):
        """Remove and return the smallest value in the min-priority queue."""
        if self.is_empty():
            raise ValueError("Priority queue is empty")
        smallest = self.data[0]
        last = self.data.pop()
        if self.data:
            self.data[0] = last
            self._bubble_down(0)
        return smallest

    def is_empty(self):
        """Return True if the min-priority queue is empty, else False."""
        return len(self.data) == 0

    def _bubble_up(self, index):
        """Restore the min-heap property by bubbling up the value at the given index."""
        parent = (index - 1) // 2
        if index > 0 and self.data[index] < self.data[parent]:
            self.data[index], self.data[parent] = self.data[parent], self.data[index]
            self._bubble_up(parent)

    def _bubble_down(self, index):
        """Restore the min-heap property by bubbling down the value at the given index."""
        left_child = 2 * index + 1
        right_child = 2 * index + 2
        smallest = index
        if left_child < len(self.data) and self.data[left_child] < self.data[smallest]:
            smallest = left_child
        if right_child < len(self.data) and self.data[right_child] < self.data[smallest]:
            smallest = right_child
        if smallest != index:
            self.data[index], self.data[smallest] = self.data[smallest], self.data[index]
            self._bubble_down(smallest)

if __name__ == '__main__':
    priority_queue_tests.run_all_tests(MyPriorityQueue)
