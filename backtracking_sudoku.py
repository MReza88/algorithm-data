from time import sleep

board = [
    [1,4,8, 0,7,0, 0,0,0],
    [9,5,7, 0,2,8, 0,0,0],
    [6,3,0, 5,9,1, 8,4,7],

    [0,9,5, 2,3,0, 0,0,1],
    [0,0,1, 0,4,6, 0,5,3],
    [0,7,0, 1,0,0, 0,0,0],

    [7,2,0, 8,6,9, 4,0,0],
    [0,1,9, 7,0,4, 0,0,0],
    [0,0,0, 0,0,0, 0,7,0]
]


def print_board(board):
    "Pretty print a 9x9 sudoku board given as 2D array."
    for row_num, row in enumerate(board):
        if row_num % 3 == 0 and row_num != 0:
            print("-"*29)
        for col_num, col in enumerate(row):
            if col_num % 3 == 0 and col_num != 0:
                print("|", end="")
            print(f" {col} " if col else "   ", end="")
        print("")
    print("")


def is_valid(board, num, row, col):
    """Check if the given number is valid in the given position in the board."""
    # Check row
    for i in range(9):
        if board[row][i] == num:
            return False

    # Check column
    for i in range(9):
        if board[i][col] == num:
            return False

    # Check square
    row_start = (row // 3) * 3
    col_start = (col // 3) * 3
    for i in range(row_start, row_start+3):
        for j in range(col_start, col_start+3):
            if board[i][j] == num:
                return False

    return True


def solve(board):
    """Solve a 9x9 sudoku by filling in all empty (0) squares in the given 2D array.

    Return True when successful or False otherwise.
    """

    for row in range(9):
        for col in range(9):
            if board[row][col] == 0:
                for num in range(1, 10):
                    if is_valid(board, num, row, col):
                        board[row][col] = num
                        print_board(board)
                        sleep(0.5)
                        if solve(board):
                            return True
                        board[row][col] = 0
                return False
    return True



print_board(board)
if solve(board):
    print("Solved!")
    print_board(board)
else:
    print("No solution.")
