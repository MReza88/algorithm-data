import math


def count_char_repeats(s: str) -> int:
    if isinstance(s, float):
        raise TypeError("Input must not be a float")
    if not type(s) == str:
        raise TypeError("not string!")
    letter = list(s)
    count = 0
    for i in range(1, len(s)):
        if letter[i] == letter[i-1]:
            count += 1
    return count


def string_similarity(str1, str2):
    """Initialize a count variable to keep track of similar characters"""
    if isinstance(str1, float) or isinstance(str2, float):
        raise TypeError("str1 and str2 must not be floats")
    if not isinstance(str1, str) or not isinstance(str2, str):
        raise TypeError("str1 and str2 must be strings")
    count = 0
    for char1, char2 in zip(str1.lower(), str2.lower()):
        if char1 == char2:
            count += 1
    return count

MINUTE = 60.0
HOUR = MINUTE * 60.0
DAY = HOUR * 24.0


def stringify_time_ago(seconds):
    """
    Convert the time difference into a string representation.

    Args:
        seconds (int): The time difference in seconds.

    Returns:
        str: A string representation of the time difference.

    Raises:
        TypeError: If seconds is not an integer or is a float/boolean.
    """
    # Check that seconds is an integer
    if not isinstance(seconds, int):
        raise TypeError("Seconds must be an integer")

    # Check that seconds is not a float or boolean
    if isinstance(seconds, float) or isinstance(seconds, bool):
        raise TypeError("Seconds must not be a float or a boolean")

    # Check if time difference is less than one minute
    if seconds < MINUTE:
        return f"{seconds} seconds"

    # Check if time difference is less than half an hour
    elif seconds < HOUR/2:
        # Calculate number of minutes rounded to the nearest whole number
        minutes = int((seconds + MINUTE/2) // MINUTE)
        if minutes == 1:
            return "1 minute"
        return f"{minutes} minutes"

    # Check if time difference is less than half a day
    elif seconds < DAY/2:
        # Calculate number of hours rounded to the nearest whole number
        hours = int((seconds + HOUR/2) // HOUR)
        if hours == 1:
            return "1 hour"
        return f"{hours} hours"

    # If time difference is at least half a day
    else:
        # Calculate number of days rounded to the nearest whole number
        days = int((seconds + DAY/2) // DAY)
        if days == 1:
            return "1 day"
        return f"{days} days"

if __name__ == '__main__':
    unittest.main()