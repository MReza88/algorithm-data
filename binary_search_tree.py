import priority_queue_tests
import sys

class BinarySearchTree:
    def __init__(self):
        """Create an empty binary search (sub)tree."""
        self.left = None
        self.right = None
        self.value = None

    def add(self, value):
        """Add `value` at the correct position to the binary search (sub)tree."""
        if self.value is None:
            self.value = value
            self.left = BinarySearchTree()
            self.right = BinarySearchTree()
        else:
            if value < self.value:
                self.left.add(value)
            else:
                self.right.add(value)
            
    def has(self, value):
        """Returns `True` if the (sub)tree contains `value`, and `False` otherwise."""
        if self.value is None:
            return False
        
        if self.value == value:
            return True
        elif value < self.value:
            if self.left is None:
                return False
            return self.left.has(value)
        else:
            if self.right is None:
                return False
            return self.right.has(value)

    def is_empty(self):
        """Returns `True` if the (sub)tree is empty, and `False` otherwise."""
        return self.value is None and self.left is None and self.right is None

    def to_list(self):
        """Returns the (sub)tree as an ordered list of values."""
        if self.value is None:
            return []
        
        left_list = self.left.to_list()
        right_list = self.right.to_list()
        
        return left_list + [self.value] + right_list

    def fetch_smallest(self):
        """Finds the smallest value in the (sub)tree, removes it from the tree
        and returns it."""
        if self.left.value == None:
            value = self.value
            self.remove(value)
            return value
        else:
            return self.left.fetch_smallest()


    def to_list(self):
        """Returns the (sub)tree as an ordered list of values."""
        if self.value is None:
            return []
        
        left_list = self.left.to_list()
        right_list = self.right.to_list()
        
        return left_list + [self.value] + right_list

    def remove(self, value):
        """Remove `value` from the (sub)tree. Returns a boolean indicating if
        the value was found."""
        if self.value is None:
            return False
        
        if self.value == value:
            if self.left.is_empty() and self.right.is_empty():
                self.value = None
                self.right = None
                self.left = None
            elif self.right.is_empty():
                self.value = self.left.value
                self.left.remove(self.left.value)
            elif self.left.is_empty():
                self.value = self.right.value
                self.right.remove(self.right.value)
            else:
                smallest = self.right.fetch_smallest()
                self.value = smallest

        elif value < self.value:
            self.left.remove(value)
        elif value > self.value:
            self.right.remove(value)

        return True

    def check(self):
        """Check if the (sub)tree is a consistent binary search tree. Raises
        an exception if a problem is found."""
        result = self._check_recurse()
        if result:
            self.print()
            result.reverse()
            raise Exception(' ➔ '.join(result))

    def _check_recurse(self):
        """Helper function for check()."""
        if self.value == None:
            if self.left != None:
                return ["Empty node should not have children", "left"]
            if self.right != None:
                return ["Empty node should not have children", "right"]
        else:
            if (self.left.value != None and self.left.value > self.value):
                return ["Left child larger than parent", "left"]
            if (self.right.value != None and self.right.value < self.value):
                return ["Right child smaller than parent", "right"]
            return self.left._check_recurse() or self.right._check_recurse()

    def print(self, indent = 0):
        """Prints the structure of the (sub)tree."""
        pre = ' '*indent + ' ➔'
        if self.value == None:
            print(pre, 'empty')
        else:
            self.left.print(indent + 4)
            print(pre, self.value)
            self.right.print(indent + 4)



if __name__ == '__main__':

    def test(*ops):
        print(f"\n\nTest: ", end="")
        bst = BinarySearchTree()
        assert(bst.is_empty())
        expected = []
        for op in ops:
            print(op, end=" ")
            if op>0:
                bst.add(op)
                expected.append(op)
            else:
                bst.remove(-op)
                expected.remove(-op)
            bst.check()
            assert(bst.is_empty() == (not expected))
        print("")
        bst.print()

        for i in range(20):
            if i in expected and not bst.has(i):
                raise Exception(f"has({i}) returned false")
            if i not in expected and bst.has(i):
                raise Exception(f"has({i}) returned true")

        out = bst.to_list()
        expected.sort()
        if expected != out:
            raise Exception(f"to_list() output ({out}) doesn't match expected expected ({expected})")


    command = sys.argv[1] if len(sys.argv) >= 2 else None

    if command == 'add':
        test(+2, +1, +3)
        test(+5, +2, +6, +1)
        test(+5, +2, +3, +1, +8, +6, +7, +9)

    elif command == 'remove':
        test(+2, +1, +3, -3) # Delete a leaf node
        test(+5, +2, +6, +1, -2) # Delete a node with one child
        test(+5, +2, +3, +1, +8, +6, +7, +9, -5) # Delete a node with two children

    elif command == 'unbalanced':
        test(+1, +2, +3, +4, +5, +6, +7, +8, +9, +10, +11, +12, +13) # Create an unbalanced tree

    elif command == 'priority':
        priority_queue_tests.run_all_tests(BinarySearchTree)

    else:
        print(f"Usage: {sys.argv[0]} {{ add | remove | unbalanced | priority }}")
