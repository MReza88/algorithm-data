from mazes import maze1, maze2
import heapq


def solve(maze):
    """Finds the shortest solution for the given `maze` from the top left square to the bottom right
    square, and mark the found path with 'o' characters. Returns a boolean indicating if a path was found.
    """

    height = len(maze)
    width = len(maze[0])
    origin = (0, 0) # (x,y)
    destination = (width-1, height-1) # (x,y)

   # Estimate the distance from each square to the destination
    dist_to_dest = [[width + height] * width for _ in range(height)]
    for y in range(height):
        for x in range(width):
            if maze[y][x] == ' ':
                dx = abs(x - destination[0])
                dy = abs(y - destination[1])
                dist_to_dest[y][x] = dx + dy

    # Keep track of the best path found to each square, and its distance
    best_path = {}
    best_path[origin] = (0, None)

    # Add the starting square to the heap
    heap = [(dist_to_dest[0][0], origin)]

    # Loop until the heap is empty or the destination is reached
    while heap:
        # Pop the square with the lowest estimated total distance so far
        _, pos = heapq.heappop(heap)
        draw_on_maze(maze, pos, '.')

        # Check if this is the destination square
        if pos == destination:
            break

        # Explore the neighboring squares
        x, y = pos
        for nx, ny in [(x-1,y), (x+1,y), (x,y-1), (x,y+1)]:
            if nx < 0 or nx >= width or ny < 0 or ny >= height:
                continue
            if maze[ny][nx] != ' ':
                continue
            neighbor = (nx, ny)
            new_cost = best_path[pos][0] + 1
            if neighbor not in best_path or new_cost < best_path[neighbor][0]:
                # Update the best path found so far to the neighbor square
                best_path[neighbor] = (new_cost, pos)
                # Add the neighbor square to the heap with its estimated total distance
                heapq.heappush(heap, (new_cost + dist_to_dest[ny][nx], neighbor))

    # Check if a path was found to the destination
    if destination not in best_path:
        return False

    # Mark the best path found with 'o' characters
    pos = destination
    while pos is not None:
        draw_on_maze(maze, pos, 'o')
        pos = best_path[pos][1]

    return True


def draw_on_maze(maze, position, char):
    x, y = position
    maze[y] = maze[y][0:x] + char + maze[y][x+1:]


def print_maze(maze):
    # Ansi escape sequences, for showing the path as red
    FG_RED = '\x1b[31m'
    FG_RESET = '\x1b[39m'
    maze_text = "\n".join(maze)
    path_len = maze_text.count("o")
    explored = maze_text.count(".")

    maze_text = maze_text.replace('o', FG_RED+'o'+FG_RESET)
    print(f"Path length: {path_len}\nOther squares explored: {explored}\n\n{maze_text}\n\n\n")


if __name__ == '__main__':
    for maze_num, maze in [(1,maze1), (2,maze2)]:
        print(f"----- Maze {maze_num} -----\n")
        if solve(maze):
            print_maze(maze)
        else:
            print("No path.\n\n")
