import os
import sys


def find_files(path):
    """Return a list of all files (excluding directories) that are within `path`. In case
    `path` is not a directory, it should just be returned as a list with just that path
    as its only element."""

    if not os.path.isdir(path):
        # Base case: `path` is a file (or at least not a directory).
        return [path]
    else:
        # Recursive case: `path` is a directory, for which any contained files/directories
        # need to be recursively searched.
        files = []
        for file_name in os.listdir(path):
            file_path = os.path.join(path, file_name)
            files.extend(find_files(file_path))
        return files


if __name__ == "__main__":
    if len(sys.argv) == 2:
        print("\n".join(find_files(sys.argv[1])))
    else:
        print(f"Usage: python files.py PATH", file=sys.stderr)
        