import unittest
from jumpmaze import generate_maze, solve, solve_with_teleports


class TestJumpMaze(unittest.TestCase):

    def test_generate_maze(self):
        # Test case for generate_maze function
        maze = generate_maze(10)
        self.assertEqual(len(maze), 10)  # Check maze size

    def test_solve(self):
        # Test case for solve function
        maze = [
            [1, 2, 1],
            [1, 0, 1],
            [1, 1, 1]
        ]
        path = solve(maze)
        self.assertIsNotNone(path)  # Check if a path is found
        self.assertEqual(len(path), 5)  # Check path length

    def test_solve_with_teleports(self):
        # Test case for solve_with_teleports function
        maze = [
            [1, 2, 1],
            [1, 0, 1],
            [1, 1, 1]
        ]
        path = solve_with_teleports(maze)
        self.assertIsNotNone(path)  # Check if a path is found
        self.assertEqual(len(path), 2)  # Check path length

    def test_generate_maze_custom_size(self):
        # Test case for generate_maze with a custom size
        size = 5
        maze = generate_maze(size)
        self.assertEqual(len(maze), size)  # Check maze size

    def test_solve_unsolvable_maze(self):
        # Test case for solve function with an unsolvable maze
        maze = [
            [1, 2, 1],
            [1, 0, 1],
            [0, 0, 1]
        ]
        path = solve(maze)
        self.assertEqual(len(path), 5)  # Check if no path is found

    def test_solve_with_teleports_custom_maze(self):
        # Test case for solve_with_teleports function with a custom maze
        maze = [
            [1, 2, 1],
            [1, 0, 1],
            [1, 1, 1]
        ]
        path = solve_with_teleports(maze)
        self.assertIsNotNone(path)  # Check if a path is found
        self.assertEqual(len(path), 2)  # Check path length

if __name__ == '__main__':
    unittest.main()