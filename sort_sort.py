import random
import time


def custom_sort(data):
    """Sorts the items in the `data` list in-place. Returns nothing."""
    n = len(data)
    for i in range(n):
        for j in range(0, n-i-1):
            if data[j] > data[j+1]:
                data[j], data[j+1] = data[j+1], data[j]


def selection_sort(data):
    """Sorts the items in the `data` list in-place. Returns nothing."""
    n = len(data)
    # Traverse through all array elements
    for i in range(n):
        # Find the minimum element in remaining unsorted array
        min_index = i
        for j in range(i+1, n):
            if data[j] < data[min_index]:
                min_index = j
        # Swap the found minimum element with the first element
        data[i], data[min_index] = data[min_index], data[i]
        

def merge_sort(data):
    """Sorts the items in the `data` list in-place. Returns nothing."""

    if len(data) < 2:
        return

    mid = len(data) // 2
    left = data[:mid]
    right = data[mid:]

    merge_sort(left)
    merge_sort(right)

    left_number = 0
    right_number = 0
    merge_index = 0
    while left_number < len(left) and right_number < len(right):
        if left[left_number] <= right[right_number]:
            data[merge_index] = left[left_number]
            left_number += 1
        else:
            data[merge_index] = right[right_number]
            right_number += 1
        merge_index += 1

    while left_number < len(left):
        data[merge_index] = left[left_number]
        left_number += 1
        merge_index += 1

    while right_number < len(right):
        data[merge_index] = right[right_number]
        right_number += 1
        merge_index += 1


def quick_sort(data):
    """Sorts the items in the `data` list in-place. Returns nothing."""

    def quick_sort_recurse(data, start, end):
        """Sorts the items in the `data` list in the range [start, end) in-place.
        Returns nothing."""
        if end - start < 2:
            return
        pivot = data[start]
        i = start + 1
        j = end - 1

        while i <= j:
            while i <= j and data[i] <= pivot:
                i += 1
            while i <= j and data[j] >= pivot:
                j -= 1
            if i < j:
                data[i], data[j] = data[j], data[i]
        
        # place pivot in correct position
        data[start], data[j] = data[j], data[start]

        quick_sort_recurse(data, start, j)
        quick_sort_recurse(data, j + 1, end)

    quick_sort_recurse(data, 0, len(data))


if __name__ == '__main__':
    # Use this to play around with your sort algorithms.
    data = [random.randint(0,999999) for _ in range(10)]
    
    start_time = time.perf_counter()
    custom_sort(data)
    print('Time: %.3fs' % (time.perf_counter() - start_time,))

    print(data)


# def measure_sorting_time(sort_func):
#     n = 1000
#     while True:
#         # Generate a list of n random numbers
#         data = [random.randint(0, 100000) for _ in range(n)]

#         # Measure how long it takes to sort the list
#         start_time = time.time()
#         sort_func(data)
#         end_time = time.time()

#         if end_time - start_time >= 1.0:
#             # Sorting took at least one second, return the number of elements sorted
#             return n

#         # If sorting took less than one second, double the size of the list and try again
#         n *= 2

# # Measure sorting time for each algorithm
# for sort_func in [custom_sort, selection_sort, merge_sort, quick_sort]:
#     n = measure_sorting_time(sort_func)
#     print(f"{sort_func.__name__} can sort {n} numbers in about one second.")