british_words_file = "british-english"
american_words_file = "american-english"


# Create a set of British English words
"""Time complexity: O(m + n) / Space complexity: O(m + n)"""
british_words = set()
with open(british_words_file, "r") as f:
    for line in f:
        british_words.add(line.strip())


# Count the number of American English words that the British don't recognize
"""Time complexity: O(m + n) / Space complexity: O(1)"""
unknown_words_count = 0
with open(american_words_file, "r") as f:
    for line in f:
        word = line.strip()
        if word not in british_words:
            unknown_words_count += 1

print(f"{unknown_words_count} American English words that the British don't recognize.")


# A script that outputs an alphabetically ordered list of words that are in both the American and British dictionaries
"""Time complexity: O(m + n) / Space complexity: O(1)"""
with open('american-english', 'r') as f1, open('british-english', 'r') as f2:
    american_words = set(word.strip() for word in f1)
    british_words = set(word.strip() for word in f2)

common_words = sorted(list(american_words.intersection(british_words)))
for word in common_words:
    print(word)


# open the American English word file and read it line by line
"""Time complexity: O(m) / Space complexity: O(m)"""
with open('american-english', 'r') as file:
    prefix_counts = {}
    for line in file:
        word = line.strip()
        if len(word) < 3:  # skip words with less than three letters
            continue
        prefix = word[:3]  # take the first three letters of the word
        if prefix in prefix_counts:
            prefix_counts[prefix] += 1
        else:
            prefix_counts[prefix] = 1

# sort the prefix counts by frequency (in descending order)
sorted_prefix_counts = sorted(prefix_counts.items(), key=lambda x: x[1], reverse=True)

# output the 20 most popular word prefixes
for prefix, count in sorted_prefix_counts[:20]:
    print(f'{prefix}: {count}')