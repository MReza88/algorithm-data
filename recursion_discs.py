def solve(source, target, count):
    """Assuming stack `source` contains `count` discs, numbered 1 (the smallest) through `count` (the biggest), 
    return a list of instructions for moving the discs to stack `target`, observing the rules:
    - Only a single disc can be moved at a time.
    - A disc can never be on top of a smaller disc.
    - One additional stack may be used.
    
    Each instruction in the list should be a tuple containing the source disc, the target disc and the 
    disc number to be moved. For example: `('A', 'B', 1)`.
    """

    if count==0:
        return []

    if count == 1:
        return [(source, target, 1)]
    else:
        temp = 'B' if source != 'B' and target != 'B' else 'C' if source != 'C' and target != 'C' else 'A'
        instructions = solve(source, temp, count-1)
        instructions.append((source, target, count))
        instructions.extend(solve(temp, target, count-1))
        return instructions

